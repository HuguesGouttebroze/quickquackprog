//import Gitlab from '@geist-ui/react-icons/gitlab'
export default {
//  projectLinkIcon: <Gitlab />,
  projectLink: 'https://github.com/shuding/nextra', // GitHub link in the navbar
  docsRepositoryBase: 'https://github.com/shuding/nextra/blob/master', // base URL for the docs repository
  titleSuffix: ' – QwickQwack',
  nextLinks: true,
  prevLinks: true,
  defaultMenuCollapsed: true,
  font: true,
  search: true,
  customSearch: null, // customizable, you can use algolia for example
  darkMode: true,
  footer: true,
  /* footerText: `MIT ${new Date().getFullYear()} © Create in "
  <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
  <path strokeLinecap="round" strokeLinejoin="round" d="M21 8.25c0-2.485-2.099-4.5-4.688-4.5-1.935 0-3.597 1.126-4.312 2.733-.715-1.607-2.377-2.733-4.313-2.733C5.1 3.75 3 5.765 3 8.25c0 7.22 9 12 9 12s9-4.78 9-12z" />
</svg>
 Hugues Gouttebroze.`, */
  footerEditLink: `Edit this page on GitHub`,
  logo: (
    <>
      <svg>...</svg>
      <span>Here into a JS programmer brain</span>
    </>
  ),
  footerText: (
    <>
      <span>
        Create in 
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
          <path strokeLinecap="round" strokeLinejoin="round" d="M21 8.25c0-2.485-2.099-4.5-4.688-4.5-1.935 0-3.597 1.126-4.312 2.733-.715-1.607-2.377-2.733-4.313-2.733C5.1 3.75 3 5.765 3 8.25c0 7.22 9 12 9 12s9-4.78 9-12z" />
        </svg>
        {new Date().getFullYear()}, a NextJS application, © Hugues Gouttebroze.
        </span>
    </>
  ),
/*   i18n: [
  { locale: 'fr', text: 'French' },
  { locale: 'en', text: 'English' },
], */
  head: (
    <>
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta name="description" content="Hugues QwickQwack coding. with NextJS" />
      <meta name="og:title" content="Nextra: the next " />
    </>
  ),
}
/* const YEAR = new Date().getFullYear()

export default {
  footer: (
    <small style={{ display: 'block', marginTop: '8rem' }}>
      <time>{YEAR}</time> ©HuguesG.
      <a href="/feed.xml">RSS</a>
      <style jsx>{`
        a {
          float: right;
        }
        @media screen and (max-width: 480px) {
          article {
            padding-top: 2rem;
            padding-bottom: 4rem;
            background: purple;
            color: white;
          }
        }
      `}</style>
    </small>
  )
}
 */
// theme for doc, instead blo
// theme.config.js
/* export default {
  projectLink: 'https://github.com/shuding/nextra', // GitHub link in the navbar
  docsRepositoryBase: 'https://github.com/shuding/nextra/blob/master', // base URL for the docs repository
  titleSuffix: ' – QwickQwack',
  nextLinks: true,
  prevLinks: true,
  search: true,
  customSearch: null, // customizable, you can use algolia for example
  darkMode: true,
  footer: true,
  footerText: `MIT ${new Date().getFullYear()} © Hugues Gouttebroze.`,
  footerEditLink: `Edit this page on GitHub`,
  logo: (
    <>
      <svg>...</svg>
      <span>Here into a JS programmer brain</span>
    </>
  ),
  head: (
    <>
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta name="description" content="Hugues QwickQwack coding. with NextJS" />
      <meta name="og:title" content="Nextra: the next " />
    </>
  ),
} */