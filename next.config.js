const withNextra = require('nextra')({
   //theme: 'nextra-theme-blog',
   theme: 'nextra-theme-docs',
   themeConfig: './theme.config.js',
   // optional: add `unstable_staticImage: true` to enable Nextra's auto image import
   unstable_staticImage: true,
   /* i18n: {
    locales: ['en', 'fr'],
    defaultLocale: 'fr' 
    },*/
   eslint: {
   // Warning: This allows production builds to successfully complete even if
   // your project has ESLint errors.
   ignoreDuringBuilds: true,
  },
})
module.exports = withNextra()
