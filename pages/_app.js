//import 'nextra-theme-blog/style.css'
import 'nextra-theme-docs/style.css'
import Head from 'next/head'

import '../styles/main.css'

export default function Nextra({ Component, pageProps }) {
  return (
    <>
      <Head></Head>
      <Component {...pageProps} />  
    </>
  )
}
