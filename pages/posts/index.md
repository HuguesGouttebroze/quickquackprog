---
type: posts
title: Posts
date: 2021-03-18
---

# NgRX - Redux pattern, Angular & RxJS

- create a store folder into `src/app`, to add interface of this list app

- Inside the store directory, create a models directory and, inside it, a `courseItem.model.ts` file. We’ll define our interface for our course list in this file:

```ts
export interface CourseItem {
  id: string;
  department: string;
  name: string;
}
```