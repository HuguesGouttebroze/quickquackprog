---
title: RxJS, Promesses vs Observables
date: 2021/3/09
description: RxJS, learn more about differences between Observables & Promises
tag: programmation fonctionnelle reactive observables rxjs async event
author: Hug
---

# Observables in Angular 2+

```js
const source = this.http.get('https://example.com');
source$.subscribe({
  next: data => handleData(data), // Success handler
  error: error => handleError(error), // Error handler
  complete: () => completeHandler(), // Complete handler
});
```
```ts
import useSWR from 'swr'
function Profile() {
  const { data, error } = useSWR('/api/user', fetcher)
  if (error) return <div>failed to load</div>
  if (!data) return <div>loading...</div>
  return <div>hello {data.name}!</div>
}
```
## Les promesses ne peuvent pas être annulées alors que les Observables peuvent être désabonnés

* Encore une fois, commencez par un exemple lorsque nous effectuons une recherche sur un backend lors d'un changement de texte d'entrée

```ts
<input ngKeyup="onKeyUp($event)">
// Promise-wrapped HTTP request
saveChanges(event) {
  const text = event.target.value;
  $http.get("https://example.com?search=" + text)
    .then(searchResult => showSearchResult(searchResult));
}
```

* Il y a un inconvénient ici - vous ne pouvez pas rejeter les résultats de la requête précédente si l'utilisateur continue à taper ( debounceréduit un peu ce problème mais ne l'élimine pas). Et encore un autre problème - les conditions de course sont possibles (lorsqu'un résultat de demande ultérieur reviendra plus rapidement qu'un précédent - nous obtenons donc une réponse incorrecte affichée).

* L'`Observable` peut éviter ce souci assez élégamment avec l' opérateur `switchMap`

```ts
<input id="search">
// Observable-wrapped HTTP requet
const inputElement = document.getElementById('search');
const search$ = fromEvent(inputElement, 'keyup');

ngOnInit() {
  search$.pipe( // each time a new text value is emitted
    switchMap(event => { // switchMap cancels previous request and sends a new one
      const text = event.target.value;

      return this.http.get('https://example.com?search=' + text);
    })
  ).subscribe(newData => this.applyNewData(newData)); // Use new data
}
```

* Nous parlerons d' opérateurs `switchMap` observables d'ordre supérieur et d'autres opérateurs dans le cours avancé sur RxJS.

# 3 Facile à prévenir les conditions de course avec Observables et difficile - avec Promises.

```ts
    getData() {

      this.http$.get('...').then(doSomeRenderingFunc)

    }

    setTimeout(2000, getData);

    setTimeout(2500, getData);
```
